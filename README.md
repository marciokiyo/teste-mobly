# Teste de desenvolvedor Mobly

Projeto feito com Laradock para a infra, Laravel 6 para aplicação e appzcoder/crud-generator para o scaffold CRUD inicial.

## Requisitos

- Docker 
- Docker Compose

## Para executar o projeto...

Para rodar o projeto, no terminal, vá para a pasta `laradock` e rode o seguinte comando:

```
docker-compose up -d nginx mysql
```

Ao terminar a construção dos containers execute o seguinte para instalar as dependencias no Composer:

```
docker-compose exec workspace bash
cd mobly
composer install
artisan migrate && artisan db:seed
```

Acesse a aplicação com localhost, com a porta padrão (80), ex:

```
http://localhost/users
```

Para consultar a API JSON, utilize o Postman, com o header:

```
Accept: application/json
```

Endpoints da API:
```
http://localhost/api/users
http://localhost/api/users/{id}
http://localhost/api/users/{id}/posts
```
