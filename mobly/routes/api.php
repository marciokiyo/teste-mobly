<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', 'MoblyUserController@index');
Route::get('/users/{id}', 'MoblyUserController@show');
Route::get('/users/{userId}/posts', 'MoblyPostController@index');
