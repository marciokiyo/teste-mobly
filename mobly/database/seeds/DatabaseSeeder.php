<?php

use App\MoblyPost;
use App\MoblyUser;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $guzzleClient = new Client();

        static::saveUsers($guzzleClient);

        static::savePosts($guzzleClient);
    }

    private static function saveUsers(Client $guzzleClient)
    {
        $client = $guzzleClient->get('http://jsonplaceholder.typicode.com/users');
        $users = json_decode($client->getBody(), true);
        foreach ($users as $user) {
            $moblyUser = new MoblyUser();
            $moblyUser->id = $user['id'];
            $moblyUser->name = $user['name'];
            $moblyUser->email = $user['email'];
            $moblyUser->username = $user['username'];
            $moblyUser->address_street = $user['address']['street'];
            $moblyUser->address_suite = $user['address']['suite'];
            $moblyUser->address_city = $user['address']['city'];
            $moblyUser->address_zipcode = $user['address']['zipcode'];
            $moblyUser->address_geo_lat = $user['address']['geo']['lat'];
            $moblyUser->address_geo_lng = $user['address']['geo']['lng'];
            $moblyUser->phone = $user['phone'];
            $moblyUser->website = $user['website'];
            $moblyUser->company_name = $user['company']['name'];
            $moblyUser->company_catch_phrase = $user['company']['catchPhrase'];
            $moblyUser->company_bs = $user['company']['bs'];
            $moblyUser->save();
        }
    }

    private static function savePosts(Client $guzzleClient)
    {
        $client = $guzzleClient->get('http://jsonplaceholder.typicode.com/posts');
        $posts = json_decode($client->getBody(), true);
        foreach ($posts as $post) {
            $moblyPost = new MoblyPost();
            $moblyPost->id = $post['id'];
            $moblyPost->mobly_user_id = $post['userId'];
            $moblyPost->title = $post['title'];
            $moblyPost->body = $post['body'];
            $moblyPost->save();
        }
    }
}
