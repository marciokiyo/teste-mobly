<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul role="tablist">
                <li role="presentation">
                    <a href="{{ url('/') }}">
                        Dashboard
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/users') }}">
                        Users
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
