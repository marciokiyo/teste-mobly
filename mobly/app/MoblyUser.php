<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoblyUser extends Model
{
    protected $fillable = [
        'name',
        'email',
        'username',
        'address_street',
        'address_suite',
        'address_city',
        'address_zipcode',
        'address_geo_lat',
        'address_geo_lng',
        'phone',
        'website',
        'company_name',
        'company_catch_phrase',
        'company_bs',
        ];

    public function posts()
    {
        return $this->hasMany(MoblyPost::class);
    }
}
