<?php

namespace App\Http\Controllers;

use App\MoblyPost;
use Illuminate\Http\Request;

class MoblyPostController extends Controller
{
    public function index($userId, Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $moblypost = MoblyPost::where('mobly_user_id', $userId)
                ->where('title', 'LIKE', "%$keyword%")
                ->orWhere('body', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $moblypost = MoblyPost::where('mobly_user_id', $userId)
                ->latest()
                ->paginate($perPage);
        }

        if ($request->wantsJson()) {
            return $moblypost;
        }

        return view('post.index', compact('moblypost', 'userId'));
    }

    public function create($userId)
    {
        return view('post.create', compact('userId'));
    }

    public function store($userId, Request $request)
    {
        $requestData = $request->all();
        $requestData['mobly_user_id'] = $userId;

        MoblyPost::create($requestData);

        return redirect("users/{$userId}/posts")->with('flash_message', 'MoblyPost added!');
    }

    public function show($userId, $id)
    {
        $moblypost = MoblyPost::findOrFail($id);

        return view('post.show', compact('moblypost', 'userId'));
    }

    public function edit($userId, $id)
    {
        $moblypost = MoblyPost::findOrFail($id);

        return view('post.edit', compact('moblypost', 'userId'));
    }

    public function update($userId, $id, Request $request)
    {
        $requestData = $request->all();

        $moblypost = MoblyPost::findOrFail($id);
        $moblypost->update($requestData);

        return redirect("users/{$userId}/posts")->with('flash_message', 'MoblyPost updated!');
    }

    public function destroy($userId, $id)
    {
        MoblyPost::destroy($id);

        return redirect("users/{$userId}/posts")->with('flash_message', 'MoblyPost deleted!');
    }
}
