<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\MoblyUser;
use Illuminate\Http\Request;

class MoblyUserController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $moblyuser = MoblyUser::where('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('username', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $moblyuser = MoblyUser::latest()->paginate($perPage);
        }
        if ($request->wantsJson()) {
            return $moblyuser;
        }

        return view('users.index', compact('moblyuser'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        MoblyUser::create($requestData);

        return redirect('users')->with('flash_message', 'MoblyUser added!');
    }

    public function show($id, Request $request)
    {
        $moblyuser = MoblyUser::findOrFail($id);
        if ($request->wantsJson()) {
            return $moblyuser;
        }
        return view('users.show', compact('moblyuser'));
    }

    public function edit($id)
    {
        $moblyuser = MoblyUser::findOrFail($id);

        return view('users.edit', compact('moblyuser'));
    }

    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $moblyuser = MoblyUser::findOrFail($id);
        $moblyuser->update($requestData);

        return redirect('users')->with('flash_message', 'MoblyUser updated!');
    }

    public function destroy($id)
    {
        MoblyUser::destroy($id);

        return redirect('users')->with('flash_message', 'MoblyUser deleted!');
    }
}
