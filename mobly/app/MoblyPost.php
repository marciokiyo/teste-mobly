<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoblyPost extends Model
{
    protected $fillable = [
        'mobly_user_id',
        'title',
        'body',
        ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
